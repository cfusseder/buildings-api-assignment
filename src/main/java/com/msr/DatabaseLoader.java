package com.msr;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.msr.data.SiteRepository;
import com.msr.data.SiteUseRepository;
import com.msr.data.UseTypesRepository;
import com.msr.model.Site;
import com.msr.model.SiteUse;
import com.msr.model.UseType;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

@Component
@Slf4j
public class DatabaseLoader implements ApplicationRunner {

  private SiteRepository siteRepository;
  private SiteUseRepository siteUseRepository;
      private UseTypesRepository useTypesRepository;
      private final ObjectMapper objectMapper;

      @Autowired
      public DatabaseLoader(SiteRepository siteRepository,
      SiteUseRepository siteUseRepository, UseTypesRepository useTypesRepository,
      ObjectMapper objectMapper) {
    this.siteRepository = siteRepository;
    this.siteUseRepository = siteUseRepository;
    this.useTypesRepository = useTypesRepository;
    this.objectMapper = objectMapper;
  }

  public void run(ApplicationArguments args) throws IOException {

    List<Site> sites = Arrays.asList(
        objectMapper.readValue(ResourceUtils.getFile("classpath:data/sites.json"), Site[].class));

    List<SiteUse> siteUses = Arrays.asList(
        objectMapper
            .readValue(ResourceUtils.getFile("classpath:data/site_uses.json"), SiteUse[].class));

    List<UseType> useTypes = Arrays.asList(
        objectMapper
            .readValue(ResourceUtils.getFile("classpath:data/use_types.json"), UseType[].class));

    siteRepository.saveAll(sites);
    useTypesRepository.saveAll(useTypes);
    siteUseRepository.saveAll(siteUses);

    log.info("system initialized successfully");
  }
}
