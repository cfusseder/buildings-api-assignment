package com.msr.model;

import javax.persistence.Table;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Lookup types
 *
 * @author Measurabl
 * @since 2019-06-11
 */
@Data
@Entity
@Table(name = "use_type")
public class UseType {

    @Id
    private int id;

    private String name;
}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    