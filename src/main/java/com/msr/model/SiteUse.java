package com.msr.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 * Site uses POJO
 *
 * @author Measurabl
 * @since 2019-06-11
 */

@Entity
@Table(name="site_use")
public class SiteUse {

    @Id
    private Integer id;

    private String description;

    private long size_sqft;

    private int site_id;

    private int use_type_id;

    @ManyToOne
    @JoinColumn(name = "site_id", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Site site;

    @ManyToOne
    @JoinColumn(name = "use_type_id", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private UseType useTypes;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getSize_sqft() {
        return size_sqft;
    }

    public void setSize_sqft(long size_sqft) {
        this.size_sqft = size_sqft;
    }

    public int getSite_id() {
        return site_id;
    }

    public void setSite_id(int site_id) {
        this.site_id = site_id;
    }

    public int getUse_type_id() {
        return use_type_id;
    }

    public void setUse_type_id(int use_type_id) {
        this.use_type_id = use_type_id;
    }
}


////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    