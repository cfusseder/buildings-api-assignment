package com.msr.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Unable to find site")
public class SiteNotFoundException extends RuntimeException {

  public SiteNotFoundException(String message) {
    super(message);
  }
}
