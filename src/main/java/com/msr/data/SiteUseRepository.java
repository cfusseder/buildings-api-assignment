package com.msr.data;

import com.msr.model.SiteUse;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository functionality for UseType
 *
 * @author Measurabl
 * @since 2019-06-12
 */
public interface SiteUseRepository extends JpaRepository<SiteUse, Integer> {

}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    