package com.msr.data;

import com.msr.exceptions.SiteNotFoundException;
import com.msr.model.Site;
import com.msr.model.UseType;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Global DAO functionality not domain driven
 *
 * @author Measurabl
 * @since 2019-06-06
 */
@Service
@Slf4j
public class SiteDao {

  @Autowired
  private SiteRepository siteRepository;

  @Autowired
  private UseTypesRepository useTypesRepository;


  public Site findSiteById(int id) {
    Optional<Site> site = siteRepository.findById(id);
    if (!site.isPresent()) {
      throw new SiteNotFoundException("can't find site");
    }
    return site.get();
  }


  public List<Site> findAllSites() {
    return (List<Site>) siteRepository.findAll();
  }

  public UseType findUseTypeById(int id) {
    Optional<UseType> useTypes = useTypesRepository.findById(id);
    if (!useTypes.isPresent()) {
      return null;
    }
    return useTypes.get();
  }

  public void saveSite(Site site) {
    siteRepository.save(site);
  }


  public void deleteSiteById(int siteId) {
    siteRepository.deleteById(siteId);
  }


}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    