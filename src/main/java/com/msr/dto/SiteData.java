package com.msr.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class SiteData {

  public int id;

  protected String name;

  protected String address;

  protected String city;

  protected String state;

  protected String zipcode;

}
