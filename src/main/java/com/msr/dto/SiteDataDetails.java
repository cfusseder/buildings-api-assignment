package com.msr.dto;

import com.msr.model.UseType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SiteDataDetails extends SiteData {

  protected long total_size;

  protected UseType primary_type;

}
