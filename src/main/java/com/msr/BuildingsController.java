package com.msr;

import com.msr.dto.SiteData;
import com.msr.model.Site;
import com.msr.service.SiteService;
import com.msr.dto.SiteDataDetails;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Respond to site requests
 *
 * @author Measurabl
 * @since 2019-06-06
 */
@RestController
@RequestMapping("/buildings")
public class BuildingsController {

  @Autowired
  private SiteService siteService;

  @GetMapping("/site/{id}")
  public SiteData findSiteById(@PathVariable int id) {

    return siteService.findSiteByIdWithUseDetails(id);
  }

  @GetMapping("/sites")
  public List<SiteData> findSites() {

    return siteService.findAllSites();
  }

  @PostMapping("/site")
  public void createSite(@RequestBody Site site) {
    siteService.createSite(site);
  }

  @DeleteMapping("/site/{id}")
  public void deleteSiteById(@PathVariable int id) {
    siteService.deleteSiteById(id);

  }

}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    