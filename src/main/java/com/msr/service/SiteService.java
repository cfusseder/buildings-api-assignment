package com.msr.service;

import com.msr.data.SiteDao;
import com.msr.dto.SiteData;
import com.msr.dto.SiteDataDetails;
import com.msr.model.Site;
import com.msr.model.SiteUse;
import com.msr.model.UseType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

@Service
@Slf4j
public class SiteService {

  @Autowired
  private SiteDao siteDao;

  public SiteData findSiteByIdWithUseDetails(int id) {
    Site site = siteDao.findSiteById(id);
    List<SiteUse> siteUses = site.getSiteUses();
    if (siteUses.isEmpty()) {
      log.info("no uses for site with id {}", id);
      return buildSiteData(site);
    }
    int use_type_id = Collections.max(siteUses, Comparator.comparing(SiteUse::getSize_sqft))
        .getUse_type_id();
    UseType useType = siteDao.findUseTypeById(use_type_id);
    long totalSizeInSqft = siteUses.stream().mapToLong(SiteUse::getSize_sqft).sum();
    return buildSiteDataDetails(site, useType, totalSizeInSqft);
  }

  public List<SiteData> findAllSites() {
    List<Site> sites = siteDao.findAllSites();
    List<SiteData> siteDataList = new ArrayList<>();

    for (Site site : sites) {
      SiteData data = buildSiteData(site);
      siteDataList.add(data);
    }

    return siteDataList;
  }

  public void createSite(@RequestBody Site site) {
    siteDao.saveSite(site);
  }

  public void deleteSiteById(@PathVariable int id) {
    siteDao.deleteSiteById(id);

  }


  private SiteDataDetails buildSiteDataDetails(Site site, UseType useType, long totalSizeInSqft) {
    return SiteDataDetails.builder().id(site.getId()).name(site.getName())
        .address(site.getAddress())
        .city(site.getCity()).state(site.getState()).zipcode(site.getZipcode())
        .total_size(totalSizeInSqft).primary_type(useType).build();
  }

  private SiteData buildSiteData(Site site) {
    return SiteData.builder().id(site.getId()).name(site.getName()).address(site.getAddress())
        .city(site.getCity()).state(site.getState()).zipcode(site.getZipcode()).build();
  }

}
