CREATE TABLE site
(
  pk      INT AUTO_INCREMENT PRIMARY KEY,
  id      INTEGER UNIQUE,
  name    VARCHAR(255),
  address VARCHAR(255),
  city    VARCHAR(255),
  state   VARCHAR(255),
  zipcode VARCHAR(255)
);


CREATE TABLE use_type
(
  pk   INT AUTO_INCREMENT PRIMARY KEY,
  id   INTEGER UNIQUE,
  name VARCHAR(255)
);

CREATE TABLE site_use
(
  pk          INT AUTO_INCREMENT PRIMARY KEY,
  id          INTEGER UNIQUE,
  site_id     INTEGER,
  use_type_id INTEGER,
  size_sqft   VARCHAR(255),
  description VARCHAR(255),
  FOREIGN KEY (site_id) REFERENCES site (id) ON DELETE CASCADE,
  FOREIGN KEY (use_type_id) REFERENCES use_type (id) ON DELETE CASCADE
);